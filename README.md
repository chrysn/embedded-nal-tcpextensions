# embedded-nal-tcpextensions

Extensions to [embedded_nal]'s TCP traits to make more precise use of TCP buffers.

This is not intended to be a long-term extension of embedded-nal,
but more an experimenation playground for [#59]
where users and implementations can play (ideally behind unstable feature gates);
once this is mature it is hoped that it'll be taken up into embedded-nal.

## Maintenance status

This crate has not been updated since embedded-hal 0.6 (as of late 2024, that trait crate is at
0.9), and is not expected to be updated any further. The experiment has shown that while it may
be possible to implement this on embedded platforms, there can be significant pushback from TCP
stack maintainers to expose this capability (see [#16850]), as it can mess with expectations
around when to ACK and what the usable vs. communicated window size is.

There may still be some way to implement this optimization, but for the time being, this is not
being followed any further.

[#59]: https://github.com/rust-embedded-community/embedded-nal/issues/59
[#16850]: https://github.com/RIOT-OS/RIOT/pull/16850

License: MIT OR Apache-2.0
